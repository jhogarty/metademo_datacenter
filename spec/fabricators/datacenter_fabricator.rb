Fabricator(:datacenter) do
  code       "ATL"
  name       "Atlanta"
  pattern    nil
  created_by "MetaDemo::Datacenter"
  created_dttm Time.now
  updated_by nil
  updated_dttm nil
end

Fabricator(:hk_dc, from: :datacenter) do
  code       "HK"
  name       "Hong Kong"
end

Fabricator(:miami_dc, from: :datacenter) do
  code       "MIA"
  name       "Miami"
end

Fabricator(:seattle_dc, from: :datacenter) do
  code       "SEA"
  name       "Seattle"
end

Fabricator(:austin_dc, from: :datacenter) do
  code       "AUS"
  name       "Austin"
end

