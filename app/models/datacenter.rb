class Datacenter < ApplicationRecord
  validates_presence_of :code, :name, :created_by, :created_dttm
end
