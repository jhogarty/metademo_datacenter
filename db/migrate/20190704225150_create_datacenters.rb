class CreateDatacenters < ActiveRecord::Migration[5.2]
  def change
    create_table :datacenters do |t|
      t.string :code
      t.string :name
      t.string :pattern
      t.string :created_by
      t.datetime :created_dttm
      t.string :updated_by
      t.datetime :updated_dttm
    end
    add_index :datacenters, :code, unique: true
    add_index :datacenters, :name, unique: true
  end
end
