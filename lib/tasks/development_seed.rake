unless ENV['RAILS_ENV'] == 'production' 
  require 'fabrication'

  namespace :dev do
    desc "Seed development database, maintain consistency"
    task prime: :environment do

      Datacenter.delete_all

      Fabricate(:datacenter)
      Fabricate(:hk_dc)
      Fabricate(:miami_dc)
      Fabricate(:seattle_dc)
      Fabricate(:austin_dc)

      puts "Created #{Datacenter.count} records for the Datacenter table."
    end
  end

end

